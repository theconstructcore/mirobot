import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.substitutions import LaunchConfiguration
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

from launch_ros.actions import Node
import xacro
import random

# this is the function launch  system will look for


def generate_launch_description():

    ####### DATA INPUT ##########
    xacro_file = "mirobot_urdf_2.xacro"
    package_description = "mirobot_description"
    ####### DATA INPUT END ##########

    robot_desc_path = os.path.join(get_package_share_directory(
        package_description), "robot", xacro_file)

    robot_desc = xacro.process_file(robot_desc_path)
    xml = robot_desc.toxml()

    # Publish Robot Desciption in String form in the topic /robot_description
    publish_robot_description = Node(
        package= package_description,
        executable='robot_description_publisher.py',
        name='robot_description_publisher',
        output='screen',
        arguments=['-xml_string', xml,
                   '-robot_description_topic', '/robot_description'
                   ]
    )

    # Robot State Publisher
    use_sim_time = LaunchConfiguration('use_sim_time', default='true')

    robot_state_publisher_node = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        parameters=[{'use_sim_time': use_sim_time, 'robot_description': xml}],
        output="screen"
    )

    start_rviz = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(get_package_share_directory(package_description), 'launch', 'start_rviz.launch.py'),
        )
    )

    # create and return launch description object
    return LaunchDescription(
        [
            publish_robot_description,
            robot_state_publisher_node,
            #start_rviz
        ]
    )
