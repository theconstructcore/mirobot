import rclpy
from rclpy.action import ActionClient
from rclpy.node import Node
from project_custom_interfaces.action import MirobotTrajectory
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import argparse


class MirobotActionClient(Node):

    def __init__(self):
        super().__init__('my_moving_robot_ac_node')

        self.declare_parameter('mode', "red")
        self.declare_parameter('x', 0.0)
        self.declare_parameter('y', 0.0)
        self.declare_parameter('z', 0.0)
        self.declare_parameter('roll', 0.0)
        self.declare_parameter('pitch', 0.0)
        self.declare_parameter('yaw', 0.0)

        
        
        self.action_complete = False

        self._action_client = ActionClient(self, MirobotTrajectory, 'my_moving_robot_as')

    def init_arg_variables(self):


        self.mode = self.get_parameter(
            'mode').get_parameter_value().string_value
        self.x = self.get_parameter(
            'x').get_parameter_value().double_value
        self.y = self.get_parameter(
            'y').get_parameter_value().double_value
        self.z = self.get_parameter(
            'z').get_parameter_value().double_value
        self.roll = self.get_parameter(
            'roll').get_parameter_value().double_value
        self.pitch = self.get_parameter(
            'pitch').get_parameter_value().double_value
        self.yaw = self.get_parameter(
            'yaw').get_parameter_value().double_value

        self.get_logger().info("############### mode...."+str(self.mode))
        self.get_logger().info("############### x...."+str(self.x))
        self.get_logger().info("############### y...."+str(self.y))
        self.get_logger().info("############### z...."+str(self.z))
        self.get_logger().info("############### roll...."+str(self.roll))
        self.get_logger().info("############### pitch...."+str(self.pitch))
        self.get_logger().info("############### yaw...."+str(self.yaw))

    def send_goal(self):
        self.init_arg_variables()
        goal_msg = MirobotTrajectory.Goal()
        goal_msg.joint_trajectory_values = JointTrajectory()
        goal_msg.mode = self.mode

        if goal_msg.mode == "tcp":
            jt_point = JointTrajectoryPoint()
            # We consider the Positions for the XYZ pos and Velocities the RPY vales
            jt_point.positions = [self.x,self.y,self.z]
            jt_point.velocities = [self.roll,self.pitch,self.yaw]
            goal_msg.joint_trajectory_values.points.append(jt_point)
        elif goal_msg.mode == "joints":
            # We consider the Positions for all the jooints values
            jt_point.positions = [self.x,self.y,self.z,self.roll,self.pitch,self.yaw]           
            goal_msg.joint_trajectory_values.points.append(jt_point)
            goal_msg.joint_trajectory_values.joint_names = ["j1",
                                                            "j2",
                                                            "j3",
                                                            "j4",
                                                            "j5",
                                                            "j6"]
        else:
            pass

        self._action_client.wait_for_server()
        self._send_goal_future = self._action_client.send_goal_async(goal_msg, feedback_callback=self.feedback_callback)

        self._send_goal_future.add_done_callback(self.goal_response_callback)
    
    def goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected :(')
            return

        self.get_logger().info('Goal accepted :)')

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)
    
    def get_result_callback(self, future):
        result = future.result().result
        self.action_complete = result.complete
        self.get_logger().info('Action complete: '+str(result.complete))
        rclpy.shutdown()

    def feedback_callback(self, feedback_msg):
        feedback = feedback_msg.feedback
        self.get_logger().info('Received feedback joints_state: '+str(feedback.joints_state))
        self.get_logger().info('Received feedback status: '+str(feedback.status))

    
def main(args=None):
    rclpy.init(args=args)

    action_client = MirobotActionClient()

    # In this version the parameters are given through teh parameters set when launching this through xml or yaml
    future = action_client.send_goal()
    rclpy.spin(action_client)

    


if __name__ == '__main__':
    main()