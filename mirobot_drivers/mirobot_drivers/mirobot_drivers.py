#!/usr/bin/env python3
# https://rirze.github.io/mirobot-py/mirobot/base_mirobot.html#mirobot.base_mirobot.BaseMirobot.set_valve
from mirobot import Mirobot
import rclpy
# import the ROS2 python dependencies
from rclpy.node import Node
from sensor_msgs.msg import JointState
import math
import time

from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor

from rclpy.action import ActionServer

from project_custom_interfaces.action import MirobotTrajectory

class MirobotDrivers(Node):

    def __init__(self, home=True, publish_joint_states=True):
        # Here we have the class constructor
        # call the class constructor
        super().__init__('mirobot_obj')

        self.m = Mirobot(portname='/dev/ttyUSB0', wait=True, debug=False)


        self.count = 0
        self.state = "waiting"
        self.mx = 202
        self.my = 0.00
        self.mz = 170

        self.a = 0.0
        self.b = 0.0
        self.c = 0.0

        self.movement_state = "unknown"

        self.flag = True

        self.cartesion_flag = False


        self.group1 = MutuallyExclusiveCallbackGroup()
        # self.group2 = MutuallyExclusiveCallbackGroup()
        # Doenst matter which Reentrant or mutual
        self.group_as = MutuallyExclusiveCallbackGroup()


        if home:
            # Check <Alarm,Angle(ABCDXYZ):0.000,0.000,0.000,0.000,0.000,0.000,0.000,Cartesian coordinate(XYZ RxRyRz):202.000,0.000,181.000,0.000,0.000,0.000,Pump PWM:0,Valve PWM:0,Motion_MODE:0>
            self.m.home_simultaneous()
            self.movement_state = "homing"
        else:
            self.get_logger().warning("No HOMING...Unlocking")
            self.unlock_robot()
        
        if publish_joint_states:
            self.joint_state_publisher_ = self.create_publisher(JointState, '/joint_states', 1)
            self.joint_state_msg = JointState()
            self.joint_state_msg.name = ["joint1",
                                        "joint2",
                                        "joint3",
                                        "joint4",
                                        "joint5",
                                        "joint6"]

            # define the timer period for 0.5 seconds
            self.timer_period = 0.05
            self.timer = self.create_timer(self.timer_period, self.robot_status_only_callback, callback_group=self.group1)
        else:
            self.get_logger().warning("NO JointStates Publishing...!!!")

        # self.movement_timer = self.create_timer(1.0, self.movement_callback, callback_group=self.group2)

        self._action_server = ActionServer(
            self, MirobotTrajectory, 'my_moving_robot_as', self.start_move_execute_callback, callback_group=self.group_as)


    def get_current_joint_states(self):
        return self.joint_state_msg

    def start_move_execute_callback(self, goal_handle):

        joint_trajectory_values = goal_handle.request.joint_trajectory_values
        self.mode = goal_handle.request.mode
        self.get_logger().warning('Executing Action goal to='+str(joint_trajectory_values))
        self.get_logger().warning('Executing Action goal mode='+str(self.mode))
        rate = self.create_rate(1)
        feedback_msg = MirobotTrajectory.Feedback()

        # We send movement
        self.count = 0
        self.move_cartesian_states()

        while self.state != "end":
            feedback_msg.status = self.state 
            feedback_msg.joints_state = self.get_current_joint_states()           
            self.move_cartesian_states()
            self.get_logger().info("Start sleep")
            goal_handle.publish_feedback(feedback_msg)
            rate.sleep()
            self.get_logger().info("End Sleep")

        goal_handle.succeed()

        result = MirobotTrajectory.Result()
        result.complete = True

        # We set mode to iddle
        self.state = "idle"

        return result


    def get_mirobot_status(self):
        return self.m.get_status()

    def robot_status_callback(self):
        self.move_cartesian_states()
        status = self.get_mirobot_status()
        self.joint_angles, self.gripper_pos = self.parse_data(status)
        print(len(self.joint_angles))
        self.get_logger().debug("Joint Angles Mirobot="+str(self.joint_angles))
        self.publish_joint_state(joint_angle_value=self.joint_angles)

    def robot_status_only_callback(self):
        status = self.get_mirobot_status()
        self.joint_angles, self.gripper_pos = self.parse_data(status)
        
        if self.joint_angles is not None:
            self.get_logger().debug("Joint Angles Mirobot="+str(self.joint_angles))
            self.get_logger().debug("Gripper Pos Mirobot="+str(self.gripper_pos))
            self.publish_joint_state(joint_angle_value=self.joint_angles)
        else:
            self.get_logger().debug("JointStates Publishing Because joint abgles NONE")

    def movement_callback(self):
        #self.publish_test_joints()
        self.move_cartesian_states()

    def publish_test_joints(self):
        self.get_logger().info("Publishing AXIS angles...")
        self.one_move(-15.0)
        time.sleep(5.0)
        self.one_move(15.0)
        time.sleep(5.0)

    def one_move(self, angle):
        self.get_logger().info("####### Publishing AXIS angles...")
        joint_values_array = [0.0,0.0,0,0,0,angle]
        self.move_joints(joint_values_array, wait=False)
        self.get_logger().info("####### PEND ublishing AXIS angles...")
        

    
    def publish_joint_state(self,joint_angle_value):
        """
        header: 
        seq: 149
        stamp: 
            secs: 15
            nsecs: 289000000
        frame_id: ''
        name: 
        - joint1
        - joint2
        - joint3
        - joint4
        - joint5
        - joint6
        position: [-1.552882267219502e-10, 8.115996763535804e-11, 2.7907454125397635e-11, -4.200728653813712e-11, 1.8030910098332242e-11, -2.710276447714932e-11]
        velocity: [8.6249830457022e-13, 3.071028962676026e-08, 2.7078833553125965e-08, 1.311748749469979e-08, 1.8097337710137876e-08, 4.649232081616516e-14]
        effort: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        """

        self.joint_state_msg.header.stamp = self.get_clock().now().to_msg()

        self.joint_state_msg.position = joint_angle_value
        self.joint_state_msg.velocity = [0.0] * len(joint_angle_value)
        self.joint_state_msg.effort = [0.0] * len(joint_angle_value)

        self.joint_state_publisher_.publish(self.joint_state_msg)

    def move_cartesian_states(self):
        
        # Set state based on counter
        print("STATE="+str(self.state)+",count="+str(self.count))


        if self.count == 0:
            self.state = "open"
        elif self.count == 1:
            self.state = "down"
        elif self.count == 5:
            self.state = "close"
        elif self.count == 6 :
            self.state = "up"
        elif self.count == 11 :
            self.state = "down"
        elif self.count == 17:
            self.state = "open"
        elif self.count == 18 :
            self.state = "up"
        elif self.count == 19:
            self.state = "end"
        else:
            self.state = "idle"


        
        if self.state == "down":
            self.mz = 20.0

            if self.mode == "yellow":
                # Yellow Cube
                self.mx = 202.0
                self.my = 0.0
                self.m.go_to_cartesian_ptp(self.mx, self.my, self.mz, self.a, self.b, self.c, speed=1.0, wait=False)
                print(self.mode+"-Cartesian=="+str(self.m.cartesian))
            elif self.mode == "red":
                # Red Cube
                self.mx = 177.0
                self.my = -80.0
                self.m.go_to_cartesian_ptp(self.mx, self.my, self.mz, self.a, self.b, self.c, speed=1.0, wait=False)
                print(self.mode+"-Cartesian=="+str(self.m.cartesian))
            else:
                print("ERROR MODE NOT SUPPORTED")


        elif self.state == "up":
            self.mz = 200.0
            
            if self.mode == "yellow":
                # Yellow Cube
                self.mx = 142.0
                self.my = 0.0
                self.m.go_to_cartesian_ptp(self.mx, self.my, self.mz, self.a, self.b, self.c, speed=1.0, wait=False)
                print(self.mode+"-Cartesian=="+str(self.m.cartesian))
            elif self.mode == "red":
                # Red Cube
                self.mx = 142.0
                self.my = -80.0
                self.m.go_to_cartesian_ptp(self.mx, self.my, self.mz, self.a, self.b, self.c, speed=1.0, wait=False)
                print(self.mode+"-Cartesian=="+str(self.m.cartesian))
            else:
                print("ERROR MODE NOT SUPPORTED")

            

        elif self.state == "open":
            self.gripper_action(action="open")
        elif self.state == "close":
            self.gripper_action(action="close")
        elif self.state == "end":
            pass
        else:
            print("Iddle State")

        self.count += 1
    

    def move_joints(self, joint_values_array, wait=True):
        """
        Array 1,2,3,4,5,6, that correspond to the axis of all tehjoints in mirabots
        except the extra ones like teh gripper

        x : Union[float, MirobotAngles]
            (Default value = None) If float, this represents the angle of axis 1. If of type MirobotAngles, then this will be used for all positional values instead.
        y : float
            (Default value = None) Angle of axis 2.
        z : float
            (Default value = None) Angle of axis 3.
        a : float
            (Default value = None) Angle of axis 4.
        b : float
            (Default value = None) Angle of axis 5.
        c : float
            (Default value = None) Angle of axis 6.
        d : float
            (Default value = None) Location of slide rail module. [WE WONT USE THIS]
        speed : int
            (Default value = None) The speed in which the Mirobot moves during this operation. (mm/s)
        wait : bool
            (Default value = None) Whether to wait for output to return from the Mirobot before returning from the function. This value determines if the function will block until the operation recieves feedback. If None, use class default BaseMirobot.wait instead. 
        """

        assert len(joint_values_array)==6, "ERROR IN LENGTH OF THE JOINT ARRAY HAS TO BE 6"

        self.m.go_to_axis(  x=joint_values_array[0],
                            y=joint_values_array[1],
                            z=joint_values_array[2],
                            a=joint_values_array[3],
                            b=joint_values_array[4],
                            c=joint_values_array[5],
                            d=0.0,
                            speed=0.1,
                            wait=wait)


    def parse_data(self,data_raw):
        """
        list of two elements:
        [PoseAngles,status]
        The Joint Vlaues are published by mirobot as follows:
        AXIS 1: i=4
        0.000,0.000,0.000,0.000,X,0.0,0.000

        AXIS 2: i = 5
        0.000,0.000,0.000,0.000,0.000,X,0.0

        AXIS 3: i = 6
        0.000,0.000,0.000,0.000,0.000,0.000,X


        AXIS 4: i = 0
        X,0.000,0.000,0.000,0.000,0.000,0.000


        AXIS 5: i = 1
        0.000,X,0.000,0.000,0.000,0.000,0.000


        AXIS 6: i = 2
        0.000,0.000,X,0.000,0.000,0.000,0.000


        """
        joint_angles = None
        gripper_pos = None 
        if data_raw is not None:
            if len(data_raw) >= 2:
                pos_data = data_raw[0]

                status_data = data_raw[1]

                # [TODO]: Please chekc the 7th joint                
                aux1 = pos_data.split(",Cartesian coordinate(XYZ RxRyRz):")
                aux1_1 = aux1[0]
                aux1_2 = aux1[1]

                # EXtract gripper pos
                gripper_pos = aux1_2.split('Valve PWM:')[1].split(',')[0]
                

                # We extract the Mode if Idle or Run
                aux2 = aux1_1.split("Angle(ABCDXYZ):")
                aux2_1 = aux2[1]
                if aux2[0] == "<Idle,":
                    self.movement_state = "idle"
                elif aux2[0] == "<Run,":
                    self.movement_state = "run"
                else:
                    self.movement_state = "unknown"

                aux3 = aux2_1.split(",")
                joint_angles_str = aux3[:7]

                
                raw_joint_angles = [math.radians(float(i)) for i in joint_angles_str]

                # Reorder joint angles for cohherent structure
                joint_angles = [raw_joint_angles[4],
                                raw_joint_angles[5],
                                raw_joint_angles[6],
                                raw_joint_angles[0],
                                raw_joint_angles[1],
                                raw_joint_angles[2]]
                

            else:
                if data_raw[0] == 'ok':
                    self.get_logger().debug("OK ONLY MESSAGE")
                else:
                    self.get_logger().error("WRONG LENGTH")
        else:
            self.get_logger().error("EMPTY RAW DATA FROM STATUS="+str(data_raw)+"=")
            

        return joint_angles, gripper_pos

    
    def gripper_action(self, action):
        """
        We send the message for opening and closing gripper
        # Close
        M3S1000M4E65 
        ok
        # Open
        M3S0M4E40
        ok
        """
        if action == "open":
            self.get_logger().warning("OPENING GRIPPER")
            #self.m.send_msg(msg="M3S0M4E40")
            self.m.set_valve(pwm=40, wait=False)
        elif action == "close":
            self.get_logger().warning("CLOSING GRIPPER")
            #self.m.send_msg(msg="M3S1000M4E65")
            self.m.set_valve(pwm=65, wait=False)
        else:
            pass

    def unlock_robot(self):
        """
        This is because we might not need to home everytime
        """
        self.m.unlock_shaft()


def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    mirobot_obj = MirobotDrivers(home=True, publish_joint_states=True)
    #mirobot_obj.publish_test_joints()
    
    executor = MultiThreadedExecutor(num_threads=2)
    executor.add_node(mirobot_obj)
        
    try:
        executor.spin()
    finally:
        executor.shutdown()
        mirobot_obj.destroy_node()
            
    
    # shutdown the ROS communication
    rclpy.shutdown()

if __name__ == '__main__':
    main()
