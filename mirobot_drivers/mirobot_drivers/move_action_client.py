import rclpy
from rclpy.action import ActionClient
from rclpy.node import Node
from project_custom_interfaces.action import MirobotTrajectory
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import argparse


class MirobotActionClient(Node):

    def __init__(self):
        super().__init__('my_moving_robot_ac_node')
        
        self.action_complete = False

        self._action_client = ActionClient(self, MirobotTrajectory, 'my_moving_robot_as')

    def send_goal(self, mode, position_list=None, rpy_list=None):
        goal_msg = MirobotTrajectory.Goal()
        goal_msg.joint_trajectory_values = JointTrajectory()
        goal_msg.mode = mode

        if goal_msg.mode == "tcp":
            jt_point = JointTrajectoryPoint()
            # We consider the Positions for the XYZ pos and Velocities the RPY vales
            jt_point.positions = position_list
            jt_point.velocities = rpy_list
            goal_msg.joint_trajectory_values.points.append(jt_point)
        elif goal_msg.mode == "joints":
            # We consider the Positions for all the jooints values
            jt_point.positions = position_list+rpy_list            
            goal_msg.joint_trajectory_values.points.append(jt_point)
            goal_msg.joint_trajectory_values.joint_names = ["j1",
                                                            "j2",
                                                            "j3",
                                                            "j4",
                                                            "j5",
                                                            "j6"]
        else:
            pass

        self._action_client.wait_for_server()
        self._send_goal_future = self._action_client.send_goal_async(goal_msg, feedback_callback=self.feedback_callback)

        self._send_goal_future.add_done_callback(self.goal_response_callback)
    
    def goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected :(')
            return

        self.get_logger().info('Goal accepted :)')

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)
    
    def get_result_callback(self, future):
        result = future.result().result
        self.action_complete = result.complete
        self.get_logger().info('Action complete: '+str(result.complete))
        rclpy.shutdown()

    def feedback_callback(self, feedback_msg):
        feedback = feedback_msg.feedback
        self.get_logger().info('Received feedback joints_state: '+str(feedback.joints_state))
        self.get_logger().info('Received feedback status: '+str(feedback.status))

    
def parse_args(args):

    parser = argparse.ArgumentParser(
        description='Arguments Parser for Mirobot Actuon Client')
    
    ################## ARGS PARSE
    parser.add_argument('-mode',
                        type=str,
                        help='Time the service will be waiting',
                        required=True)
    
    # Non compulsory values

    parser.add_argument('-x', 
                        type=float,
                        default=0.0,                           
                        help="Depending on Mode, useles, X TCP or Joint 1 angle")
    parser.add_argument('-y', 
                        type=float,
                        default=0.0,                           
                        help="Depending on Mode, useles, Y TCP or Joint 2 angle")
    parser.add_argument('-z', 
                        type=float,
                        default=0.0,                           
                        help="Depending on Mode, useles, Z TCP or Joint 3 angle")
    parser.add_argument('-roll', 
                        type=float,
                        default=0.0,                           
                        help="Depending on Mode, useles, ROLL X TCP or Joint 4 angle")
    parser.add_argument('-pitch', 
                        type=float,
                        default=0.0,                           
                        help="Depending on Mode, useles, PITCH Y TCP or Joint 5 angle")
    parser.add_argument('-yaw', 
                        type=float,
                        default=0.0,                           
                        help="Depending on Mode, useles, YAW Z TCP or Joint 6 angle")

    
    new_args = parser.parse_args(args[1:])

    mode = new_args.mode
    x = float(new_args.x)
    y = float(new_args.y)
    z = float(new_args.z)
    roll = float(new_args.roll)
    pitch = float(new_args.pitch)
    yaw = float(new_args.yaw)

    position_list = [x,y,z]
    rpy_list = [roll, pitch, yaw]

    ##################

    return mode, position_list, rpy_list

def main(args=None):
    rclpy.init(args=args)
    # Use: ros2 run mirobot_drivers move_action_client_exe mode:=red
    print("args==="+str(args))
    # We format the arguments given through ROS to be able to use the arguments
    args_without_ros = rclpy.utilities.remove_ros_args(args)
    print("clean ROS args==="+str(args_without_ros))

    action_client = MirobotActionClient()

    mode, position_list, rpy_list = parse_args(args_without_ros)

    future = action_client.send_goal(mode, position_list, rpy_list)
    rclpy.spin(action_client)

    


if __name__ == '__main__':
    main()