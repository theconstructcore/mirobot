import launch
from launch_ros.actions import Node

# How to use Example:
# ros2 launch mirobot_drivers start_action_client.launch.py mode:=red


def generate_launch_description():
    return launch.LaunchDescription([
        launch.actions.DeclareLaunchArgument('mode', default_value='yellow'),
        launch.actions.DeclareLaunchArgument('x', default_value='0.0'),
        launch.actions.DeclareLaunchArgument('y', default_value='0.0'),
        launch.actions.DeclareLaunchArgument('z', default_value='0.0'),
        launch.actions.DeclareLaunchArgument('roll', default_value='0.0'),
        launch.actions.DeclareLaunchArgument('pitch', default_value='0.0'),
        launch.actions.DeclareLaunchArgument('yaw', default_value='0.0'),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('mode')),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('x')),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('y')),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('z')),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('roll')),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('pitch')),
        launch.actions.LogInfo(msg=launch.substitutions.LaunchConfiguration('yaw')),
        # All the arguments have to be strings, floats will give error of NonItreable
        Node(
            package='mirobot_drivers',
            executable='mirobot_action_client_exe',
            output='screen',
            emulate_tty=True,
            arguments=["-mode", launch.substitutions.LaunchConfiguration('mode'),
                        "-x", launch.substitutions.LaunchConfiguration('x'),
                        "-y", launch.substitutions.LaunchConfiguration('y'),
                        "-z", launch.substitutions.LaunchConfiguration('z'),
                        "-roll", launch.substitutions.LaunchConfiguration('roll'),
                        "-pitch", launch.substitutions.LaunchConfiguration('pitch'),
                        "-yaw", launch.substitutions.LaunchConfiguration('yaw'),
                    ]
            ),
    ])