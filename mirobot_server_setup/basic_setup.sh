#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

BLUETEXT="\033[1;34m"
REDTEXT="\033[1;31m"
WHITETEXT="\033[0;37m"
GREENTEXT="\033[1;32m"

function ros2_install {


    locale  # check for UTF-8

    sudo apt update && sudo apt install -y locales
    sudo locale-gen en_US en_US.UTF-8
    sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
    export LANG=en_US.UTF-8

    locale  # verify settings

    sudo apt install -y software-properties-common
    sudo add-apt-repository universe

    sudo apt update && sudo apt install -y curl gnupg lsb-release
    sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(source /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

    sudo apt install -y ros-galactic-ros-base

    source /opt/ros/galactic/setup.bash
    mkdir -p ~/ros2_ws/src
    cd ~/ros2_ws

    sudo apt install -y python3-colcon-common-extensions

    colcon build


    echo -e "$BLUETEXT\n/ROS2 Install successfully"
    echo -e "\n$WHITETEXT"

}


function ros2_env_setup {

    source ~/ros2_ws/install/setup.bash

    echo -e "$BLUETEXT\n/ROS2 Environment setup successfully"
    echo -e "\n$WHITETEXT"

}

function remotedesktop {

    sudo apt install xrdp 
    sudo systemctl status xrdp
    sudo adduser xrdp ssl-cert 
    sudo systemctl restart xrdp

    sudo ufw allow from 192.168.1.137/24 to any port 3389

}

function mirobot_env_setup {

    sudo apt install python3-pip
    pip install mirobot-py
    # Check which use is used
    sudo chmod 777 /dev/ttyUSB0

    

    source ~/ros2_ws/install/setup.bash
    cd ~/ros2_ws/src/
    git clone https://bitbucket.org/theconstructcore/mirobot.git
    cd ~/ros2_ws
    rosdep install -i --from-path src --rosdistro dashing -y
    colcon build --symlink-install
    source ~/ros2_ws/install/setup.bash

    echo -e "$BLUETEXT\n/Mirobot Environment setup successfully"
    echo -e "\n$WHITETEXT"
}

function main {
    set -x
    
    ros2_install
    ros2_env_setup
    mirobot_env_setup

    set +x
}

main
echo -e "$GREENTEXT\nFinished Mirobot Basic Install"
echo -e "$GREENTEXT\n/Mark is: $MARK"
echo -e "\n$WHITETEXT"
