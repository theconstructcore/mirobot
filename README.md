**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

This is a ROS2 port for the MiroBot Wlkata robot arm.
---

## Demo


ros2 launch mirobot_description start_robot_state_pub.launch.py
ros2 launch mirobot_description start_rviz.launch.py
ros2 run mirobot_drivers mirobot_drivers_exe